# ACF Encrypted Field

## Description
The ACF Encrypted Field plugin adds a field to Advanced Custom Fields that is saved encrypted to the database and returned decrypted in plain text format


## Features
* Add a custom Encrypted Field to Advanced Custom Fields
* The default encryption key can be changed with the 'acf/fields/encrypted/key' filter


## Installation:

Add repository to your local composer file:

    {
        "type": "git",
        "url": "https://joldnl@bitbucket.org/joldnl/jold-acf-encrypted-field.git"
    }



Add the plugin as a depenency:

    "joldnl/jold-acf-encrypted-field": "~1.1",


Update composer by running:

    $ composer update.
